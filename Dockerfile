FROM liabifano/executor:bd0ce94

COPY setup.py /job-skeleton/
COPY requirements.txt /job-skeleton/
COPY src/ /job-skeleton/src/

RUN find . | grep -E "(__pycache__|\.pyc$)" | xargs rm -rf
RUN pip install -U -r job-skeleton/requirements.txt
RUN pip install job-skeleton/.
